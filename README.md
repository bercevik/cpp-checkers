# Hra Dáma (Checkers)

## Zadání
Implementujte hru Dáma pro dva hráče.
Hráči se po každém tahu vystřídají.
Nejdříve vyberou souřadnice své figurky a pak souřadnice místa kam se chtejí posunout.
Hra končí až jeden ze hráčú přijde o všechny své figurky
Hra sleduje momentální stav, jakmile zjistí výsledek vypíše ho do terminál.

## Dokumentace
Podrobnou dokumentaci je možné najít v složce html pod jménem index.html

## Spuštění
Hra začína tahem bíleho hráče. Každý hráč má 12 figurek.
Cílem hry je zbavit protivníka všech jeho figurek

## Ovládání programu
Hra vždy vipíše, který hráč je momentálne na tahu.
Pokuď se hráč pokusí udělat nepovolený pohyb, hra ho upozorní 
a dovolí mu pokusit se figurkov táhnout ještě jednou.

Ukončení hry - zadání `q` umožní okamžité ukončení hry
Nápovedy - zadání `h` umožní okamžité ukončení hry

### Zadávání souřadnic
Souřadnice zadáváme jako čtvěrici `[začáteční řádek, začáteční sloupec, konečný řádek, konečný sloupec]`. 
Čísla pro řádky a sloupce je možné vidět pri hrací desce, jsou číslovány od 0 do 7.
Začáteční řádek a sloupec slouží na výber figurky kterou chce hráč pohnout.
Konečný řádek a sloupec slouží na výber políčka na které chceme figurkou pohnout.
Na vyhození nepřítelovy figurky je potrěba zadat jako konečný rádek a konečný sloupec
souřadnice oné figurky, také za ní musí být volné místo na které 'skočíte'
Jestli splníte oboje podmíny múžete vyhodit nepříteli figurku


### Ukončení programu
Hra končí automaticky, a to tehdy, když kterýkoliv z hráčú stratí všechny figurky.

Program je možné kdykoliv ukončit klávesou `q`.

## Testování programu

### Dáma (deska 8*8, figurky 12 bíle a 12 černé):
-	`q`	- vypnutí hry
-	`h`	- nápovedy ke hry
-	`2 2 3 3`	- pohyb figurkou
-	`2 2 3 3, 5 1 4 2, 3 3 4 2`	- vyhození černé figurky
-	`2 2 3 3, 5 1 4 2, 3 3 4 2, 2 4 3 3, 6 0 5 1`	- vyhození bílé figurky
-	`2 2 3 3, 5 1 4 2, 3 3 4 2, 2 4 3 3, 6 0 5 1, 4 2 3 3`	- vyhození bílé figurky
-	`2 2 3 3, 5 1 4 2, 3 3 4 2, 2 4 3 3, 6 0 5 1, 4 2 3 3, 5 3 4 2`	- vyhození  dvou bílích figurek za ten samý tah

Hra byla také testována nástrojem Dr. Memory



