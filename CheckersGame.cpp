#include "CheckersGame.hpp"
#include <iostream>
#include <vector>
#include <iomanip>
#include <limits>
#include <mutex>
#include <thread>
#include <condition_variable>
using namespace std;

const int BOARD_SIZE = 8;
int WHITE_PIECES = 12;
int BLACK_PIECES = 12;
enum class PieceType { EMPTY, BLACK, WHITE };
enum class PieceRank { NONE, PAWN, QUEEN };
/**
 * @Piece
 *
 * A piece on the playing board.
 */
struct Piece {
    PieceType type;
    PieceRank rank;
};
/**
 * @CheckersGame
 *
 * The main class of the game.
 */
class CheckersGame {
public :/**
 * @CheckersGame
 *
 * Initializes the game.
 */
    CheckersGame() : board(BOARD_SIZE, std::vector<Piece>(BOARD_SIZE, {PieceType::EMPTY, PieceRank::NONE})), isPlayer1Turn(true), gameStillPlaying(true) {
        initializeBoard();
    };

/**
 * @initializeBoard
 *
 * Initializes the game board.
 */
    void initializeBoard() {
        for (int row = 0; row < BOARD_SIZE; ++row) {
            for (int col = 0; col < BOARD_SIZE; ++col) {
                if ((row + col) % 2 == 0) {
                    if (row < 3) {
                        board[row][col] = {PieceType::WHITE, PieceRank::PAWN};
                    } else if (row > BOARD_SIZE - 4) {
                        board[row][col] = {PieceType::BLACK, PieceRank::PAWN};
                    }
                }
            }
        }
//        board[2][2] = {PieceType::WHITE, PieceRank::PAWN};
//        board[4][4] = {PieceType::BLACK, PieceRank::PAWN};
    }
/**
 * @printBoard
 *
 * Prints out the game board.
 */
    void printBoard() const {
//        cout << ANSI_CLEAR;
        std::cout << "  ";
        for (int col = 0; col < BOARD_SIZE; ++col) {
            std::cout << std::setw(3) << col << " ";
        }
        std::cout << "\n";

        for (int row = 0; row < BOARD_SIZE; ++row) {
            std::cout << row << " |";

            for (int col = 0; col < BOARD_SIZE; ++col) {
                if ((row + col) % 2 == 0) {
                    switch (board[row][col].type) {
                        case PieceType::EMPTY:
                            std::cout << " . |";
                            break;
                        case PieceType::BLACK:
                            std::cout << " B |";
                            break;
                        case PieceType::WHITE:
                            std::cout << " W |";
                            break;
                    }
                } else {
                    std::cout << "   |";
                }
            }
            std::cout << '\n';

            if (row < BOARD_SIZE - 1) {
                std::cout << "   ";
                for (int col = 0; col < BOARD_SIZE; ++col) {
                    std::cout << "----";
                }
                std::cout << "\n";
            }
        }

        std::cout << "  ";
        for (int col = 0; col < BOARD_SIZE; ++col) {
            std::cout << std::setw(3) << col << " ";
        }
        std::cout << "\n";
    }

/**
 * @isValidMove
 *
 * Checks if the move is valid.
 */
    bool isValidMove(int startRow, int startCol, int endRow, int endCol) const {
        if (startRow < 0 || startRow >= BOARD_SIZE) {
            return false;
        }

        if (startCol < 0 || startCol >= BOARD_SIZE) {
            return false;
        }

        if (endRow < 0 || endRow >= BOARD_SIZE) {
            return false;
        }

        if (endCol < 0 || endCol >= BOARD_SIZE) {
            return false;
        }

        if (board[startRow][startCol].type == PieceType::EMPTY) {
            return false;
        }

        if (startRow + 1 != endRow && startRow - 1 != endRow) {
            if(board[startRow][startCol].rank != PieceRank::QUEEN) {
                return false;
            }
        }

        if (board[startRow][startCol].type == PieceType::WHITE && !isPlayer1Turn) {
            return false;
        }

        if (board[startRow][startCol].type == PieceType::BLACK && isPlayer1Turn) {
            return false;
        }

        if(startRow == endRow) {
            return false;
        }

        if (isPlayer1Turn && startRow > endRow) {
            return false;
        }

        if (!isPlayer1Turn && startRow < endRow) {
            return false;
        }

//        if (board[endRow][endCol].type != PieceType::EMPTY) {
//            return false;
//        }

        return true;
    }
/**
 * @rankUpPossible
 *
 * Checks if promoting a piece is possible.
 */
    bool rankUpPossible(int endRow) const {
        PieceType opponentPieceType = getOpponentPieceType();
        if(opponentPieceType == PieceType::BLACK){
            if(endRow == 7) {
                return true;
            }
        }
        if(opponentPieceType == PieceType::WHITE){
            if(endRow == 0) {
                return true;
            }
        }
        return false;
    }


/**
 * @getOpponentPieceType
 *
 * Checks the piece tzpe of the opponent.
 */
    PieceType getOpponentPieceType() const {
        return (isPlayer1Turn) ? PieceType::BLACK : PieceType::WHITE;
    }
/**
 * @eliminatePiece
 *
 * eliminates the piece
 */
    void eliminatePiece(int endRow, int endCol) {
        if(isPlayer1Turn) {
            BLACK_PIECES = BLACK_PIECES -1;
        } else {
            WHITE_PIECES = WHITE_PIECES -1;
        }
        board[endRow][endCol] = {PieceType::EMPTY, PieceRank::NONE};
    }
/**
 * @checkJumpPossibility
 *
 * Checks if a jump is possible.
 */
    int checkJumpPossibility(int startRow, int startCol, int endRow, int endCol) const {
        if(board[endRow][endCol].type == PieceType::EMPTY) {
            return 0;
        }

        PieceType opponentPieceType = getOpponentPieceType();
        if(board[endRow][endCol].type == opponentPieceType) {
            if(startRow < endRow) {
                //I am white 2 2/ 3 1/ 4 0
                if(startCol > endCol) {
                    if(board[endRow + 1][endCol - 1].type == PieceType::EMPTY) {
                        //jump possible
                        return 1;
                    }
                }
                if(startCol < endCol) {
                    if(board[endRow + 1][endCol + 1].type == PieceType::EMPTY) {
                        //jump possible
                        return 2;
                    }
                }
            }
            if(startRow > endRow) {
                //I am black
                if(startCol > endCol) {
                    if(board[endRow - 1][endCol - 1].type == PieceType::EMPTY) {
                        //jump possible
                        return 3;
                    }
                }
                if(startCol < endCol) {
                    if(board[endRow - 1][endCol + 1].type == PieceType::EMPTY) {
                        //jump possible
                        return 4;
                    }
                }
            }
        }
        return 0;
    }




/**
 * @makeMove
 *
 * moves the piece
 */
    void makeMove(int startRow, int startCol, int endRow, int endCol) {
        if(rankUpPossible(endRow)) {
            board[startRow][startCol].rank = PieceRank::QUEEN;
        }
        std::swap(board[startRow][startCol], board[endRow][endCol]);
        isPlayer1Turn = !isPlayer1Turn;
    }
/**
 * @isTileEmpty
 *
 * Checks if tile is empty.
 */
    bool isTileEmpty(int endRow, int endCol) {
        if(board[endRow][endCol].type == PieceType::EMPTY) {
            return true;
        }
        return false;
    }
/**
 * @isGameOver
 *
 * Checks if the game is over.
 */
    static bool isGameOver() {
        if(WHITE_PIECES == 0 || BLACK_PIECES == 0) {
            return true;
        }
        return false;
    }

//    int isGameOver() const {
//        bool whiteWin = false, blackWin = false;
//        int bPieces, wPieces;
//        for (int row = 0; row < 8; ++row) {
//            for (int col = 0; col < 8; ++col) {
//                if (board[row][col].type == PieceType::WHITE) {
//                    wPieces++;
//                }
//                if (board[row][col].type == PieceType::BLACK) {
//                    bPieces++;
//                }
//            }
//        }
//        if (bPieces == 0) {
//            whiteWin = true;
//            return 1;
//        }
//        if (wPieces == 0) {
//            blackWin = true;
//            return 2;
//        }
//
//        return 0;
//    }
/**
 * @isInputValid
 *
 * Checks if the input is valid.
 */
    static bool isInputValid(int value) {
        return value >= 0 && value <= 7;
    }
/**
 * @areInputsNumbers
 *
 * Checks if the inputs are numbers.
 */
    static bool areInputsNumbers(int startRow, int startCol, int endRow, int endCol) {
        return isInputValid(startRow) && isInputValid(startCol) && isInputValid(endRow) && isInputValid(endCol);
    }
/**
 * @play
 *
 * Starts the threads and the game.
 */
    void play() {
        std::thread inputThread(&CheckersGame::inputThread, this);
        std::thread computeThread(&CheckersGame::computeThread, this);
        std::thread outputThread(&CheckersGame::outputThread, this);

        inputThread.join();
        computeThread.join();
        outputThread.join();
    }

//    void play() {
//        while (!isGameOver()) {
//            printBoard();
//
//            int startRow, startCol, endRow, endCol;
//
//            std::cout << (isPlayer1Turn ? "Player 1's turn (W): " : "Player 2's turn (B): ");
//            std::cout << "Enter start row, start column, end row, and end column (space-separated): ";
//
//            if (std::cin >> startRow >> startCol >> endRow >> endCol && areInputsNumbers(startRow, startCol, endRow, endCol)) {
//                if (isValidMove(startRow, startCol, endRow, endCol)) {
//                    if(!isTileEmpty(endRow, endCol)) {
//                        if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 1){
//                            eliminatePiece(endRow, endCol);
//                            makeMove(startRow, startCol, endRow + 1, endCol - 1);
//                            std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
//                        }
//                        if(checkJumpPossibility(startRow, startCol, endRow + 1, endCol + 1) == 2){
//                            eliminatePiece(endRow, endCol);
//                            makeMove(startRow, startCol, endRow, endCol);
//                            std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
//                        }
//                        if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 3){
//                            eliminatePiece(endRow, endCol);
//                            makeMove(startRow, startCol, endRow - 1, endCol - 1);
//                            std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
//                        }
//                        if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 4){
//                            eliminatePiece(endRow, endCol);
//                            makeMove(startRow, startCol, endRow - 1, endCol + 1);
//                            std::cout << "Splendid move. You successfully eliminated one of your opponents pieces. 3 5\n";
//                        }
//                        if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 0){
//                            makeMove(startRow, startCol, endRow, endCol);
//                            std::cout << "MOVE ";
//
//                        }
//                    } else {
//                        makeMove(startRow, startCol, endRow, endCol);
//                    }
//
//                } else {
//                    std::cout << "Invalid move. Try again.\n";
//                }
//            } else {
//                std::cout << "Invalid input. Please enter valid numbers between 0 and 7.\n";
//                std::cin.clear();
//                std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
//            }
//        }
//
//        printBoard();
//        std::cout << (!isPlayer1Turn ? "Player 2 (B) wins!\n" : "Player 1 (W) wins!\n");
//
//    }
/**
 * @getInput
 *
 * Gets input.
 */
    void getInput(){
        if(!gameStillPlaying){
            return;
        }
        char firstChar;
//        char quit;
        cin >> firstChar;
        if (firstChar == 'q'){
            cout << "Ending game. Thanks for playing!\n" << endl;
            this_thread::sleep_for(chrono::seconds(1));
            gameStillPlaying = false;
            outputVar.notify_one();
            return;
        }

        if (firstChar == 'h'){
            cout << "\"This is a help message. Provide information about how to use the program.\n" << endl;
            cout << "\"StartRow and startCol indicate the tile from which you wish to move from\n" << endl;
            cout << "\"This tile must contain a piece of your color for it to be a valid move\n" << endl;
            cout << "\"End Row and endCol indicate the tile to which you wish to move\n" << endl;
            cout << "\"For it to be a valid tile it has to be either empty\n" << endl;
            cout << "\"or it must contain a piece of the opposing players color\n" << endl;
            cout << "\"in which case the tile beyond the opposing players piece must be empty\n" << endl;
            cout << "\"for you to be able to jump over his piece and eliminate it\n" << endl;
            this_thread::sleep_for(chrono::seconds(1));
            return;
        }



        int startRow = firstChar - '0';
        int startCol, endRow, endCol;


        if (std::cin >> startCol >> endRow >> endCol && areInputsNumbers(startRow, startCol, endRow, endCol)) {
            std::cout << startRow;
            std::cout << startCol;
            std::cout << endRow;
            std::cout << endCol;
            if (isValidMove(startRow, startCol, endRow, endCol)) {
                if(!isTileEmpty(endRow, endCol)) {
                    if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 1){
                        eliminatePiece(endRow, endCol);
                        makeMove(startRow, startCol, endRow + 1, endCol - 1);
                        std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
                        outputVar.notify_one();
                    }
                    if(checkJumpPossibility(startRow, startCol, endRow + 1, endCol + 1) == 2){
                        eliminatePiece(endRow, endCol);
                        makeMove(startRow, startCol, endRow, endCol);
                        std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
                        outputVar.notify_one();
                    }
                    if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 3){
                        eliminatePiece(endRow, endCol);
                        makeMove(startRow, startCol, endRow - 1, endCol - 1);
                        std::cout << "Splendid move. You successfully eliminated one of your opponents pieces.\n";
                        outputVar.notify_one();
                    }
                    if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 4){
                        eliminatePiece(endRow, endCol);
                        makeMove(startRow, startCol, endRow - 1, endCol + 1);
                        std::cout << "Splendid move. You successfully eliminated one of your opponents pieces. 3 5\n";
                        outputVar.notify_one();
                    }
                    if(checkJumpPossibility(startRow, startCol, endRow, endCol) == 0){
                        makeMove(startRow, startCol, endRow, endCol);
                        std::cout << "Splendid move. You successfully moved a piece ! \n";
                        outputVar.notify_one();
                    }
                } else {
                    makeMove(startRow, startCol, endRow, endCol);
                    std::cout << "Splendid move. You successfully moved a piece ! \n";
                    outputVar.notify_one();
                }

            } else {
                std::cout << "Invalid move. Try again.\n";
            }
            outputVar.notify_one();
        } else {
            std::cout << "Invalid input. Please enter valid numbers between 0 and 7.\n";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        }
    }


/**
 * @inputThread
 *
 * deals with input.
 */
    void inputThread(){
        unique_lock<mutex> lock(inputMutex);
        cout << "Input thread started.\n" << endl;

        while(gameStillPlaying){
            inputVar.wait(lock);
            cout << "waiting for input\n";
            this_thread::sleep_for(chrono::milliseconds(200));
            getInput();
        }
    }
/**
 * @outputThread
 *
 * deals with output.
 */
    void outputThread(){
        unique_lock<mutex> lock(outPutMutex);
        cout << "Output thread started.\n" << endl;

        while(gameStillPlaying){
            outputVar.wait(lock);

            this_thread::sleep_for(chrono::milliseconds(200));
            printBoard();

            std::cout << (isPlayer1Turn ? "Player 1's turn (W): \n" : "Player 2's turn (B): \n");
            std::cout << "Enter start row, start column, end row, and end column (space-separated): \n";
        }
    }
/**
 * @computeThread
 *
 * deals with computing.
 */
    void computeThread() {
        this_thread::sleep_for(chrono::milliseconds(200));
        cout << "Compute thread started\n" << endl;
        outputVar.notify_one();
        inputVar.notify_one();

        while(gameStillPlaying){
            inputVar.notify_one();
            if(isGameOver()) {
                if (isPlayer1Turn){
                    this_thread::sleep_for(chrono::milliseconds(200));
                    cout << "White won! Press q to exit the game!\n";
                    this_thread::sleep_for(chrono::seconds(5));
                    gameStillPlaying = false;
                } else if (!isPlayer1Turn){
                    this_thread::sleep_for(chrono::milliseconds(200));
                    cout << "Black won! Press q to exit the game!\n";
                    this_thread::sleep_for(chrono::seconds(5));
                    gameStillPlaying = false;
                } else {
                    cout << "Sleeping" << endl;
                    this_thread::sleep_for(chrono::milliseconds(200));
                }
            }


            this_thread::sleep_for(chrono::milliseconds(200));
        }

        outputVar.notify_all();
        inputVar.notify_all();
    }
/**
 * @printWelcomeMessage
 *
 * Prints out welcome message.
 */
    void printWelcomeMessage() {
        std::cout << R"(
 .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.   .----------------.  .----------------.   .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.
| .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. | | .--------------. || .--------------. | | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
| | _____  _____ | || |  _________   | || |   _____      | || |     ______   | || |     ____     | || | ____    ____ | || |  _________   | | | |  _________   | || |     ____     | | | |     ______   | || |  ____  ____  | || |  _________   | || |     ______   | || |  ___  ____   | || |  _________   | || |  _______     | || |    _______   | |
| ||_   _||_   _|| || | |_   ___  |  | || |  |_   _|     | || |   .' ___  |  | || |   .'    `.   | || ||_   \  /   _|| || | |_   ___  |  | | | | |  _   _  |  | || |   .'    `.   | | | |   .' ___  |  | || | |_   ||   _| | || | |_   ___  |  | || |   .' ___  |  | || | |_  ||_  _|  | || | |_   ___  |  | || | |_   __ \    | || |   /  ___  |  | |
| |  | | /\ | |  | || |   | |_  \_|  | || |    | |       | || |  / .'   \_|  | || |  /  .--.  \  | || |  |   \/   |  | || |   | |_  \_|  | | | | |_/ | | \_|  | || |  /  .--.  \  | | | |  / .'   \_|  | || |   | |__| |   | || |   | |_  \_|  | || |  / .'   \_|  | || |   | |_/ /    | || |   | |_  \_|  | || |   | |__) |   | || |  |  (__ \_|  | |
| |  | |/  \| |  | || |   |  _|  _   | || |    | |   _   | || |  | |         | || |  | |    | |  | || |  | |\  /| |  | || |   |  _|  _   | | | |     | |      | || |  | |    | |  | | | |  | |         | || |   |  __  |   | || |   |  _|  _   | || |  | |         | || |   |  __'.    | || |   |  _|  _   | || |   |  __ /    | || |   '.___`-.   | |
| |  |   /\   |  | || |  _| |___/ |  | || |   _| |__/ |  | || |  \ `.___.'\  | || |  \  `--'  /  | || | _| |_\/_| |_ | || |  _| |___/ |  | | | |    _| |_     | || |   `.____.'   | | | |   `._____.'  | || | |____||____| | || | |_________|  | || |   `._____.'  | || | |____||____| | || | |_________|  | || | |____| |___| | || |  |_______.'  | |
| |              | || |              | || |              | || |   `._____.'  | || |   `.____.'   | || ||_____||_____|| || | |_________|  | | | '--------------' || '--------------' | | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
| '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' | '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
 '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'   '----------------'  '----------------'   '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
)";
    }



private :
    std::vector<std::vector<Piece>> board;
    bool isPlayer1Turn;
    std::mutex inputMutex;
    std::mutex outPutMutex;
    std::condition_variable outputVar;
    std::condition_variable inputVar;
    bool gameStillPlaying;

};





/**
 * @main
 *
 * Main.
 */
int main() {
    CheckersGame game;
//    game.printWelcomeMessage();
    std::thread inputThread(&CheckersGame::inputThread, &game);
    std::thread computeThread(&CheckersGame::computeThread, &game);
    std::thread outputThread(&CheckersGame::outputThread, &game);

    inputThread.join();
    computeThread.join();
    outputThread.join();

    system("PAUSE");
    return 0;
}

